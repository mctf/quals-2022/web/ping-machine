<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Echelon">
    <title>Лабораторная работа "ping machine"</title>
    <!-- Bootstrap Core CSS -->
    <link href="./css/bootstrap.min.css" rel="stylesheet">
    <!-- font-awesome Core CSS -->
    <link href="./css/font-awesome.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="./css/heroic-features.css" rel="stylesheet">
    <link rel="stylesheet" href="./css/bootstrap-submenu.css">
    <script src="./js/bootstrap-submenu.js" defer></script>
</head>
<body>
    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" >Лабораторная работа №6. Ping</a>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
    <!-- Page Content -->
    <div class="container">
        <!-- Jumbotron Header -->
        <header class="jumbotron hero-spacer">
            <h2><b>Ping.</b></h2>
            <p>Ping — утилита для проверки целостности и качества соединений в сетях на основе TCP/IP, а также обиходное наименование самого запроса. Утилита отправляет запросы протокола ICMP указанному узлу сети и фиксирует поступающие ответы.</p>
        </header>

        <!-- /.row -->
        <!-- Page Features -->
        <div class="container">
          <div class="row">
              <div class="jumbotron hero-spacer">
                <form action="index.php" method="POST">
                Ping адрес: <input type="text" name="addr">
                <input value="Отправить" type="submit">
                </form>
                <br>
                <b>
                <?php
                $addr = $_POST['addr'];
                if(isset($addr)){
                    
                    if(preg_match('/ /',$addr)){
                        die("Неверный формат, возможно наличие пробелов.");
                        }else{
                        
                        if(!(preg_match('/&|\||;|\$/',$addr))){
                            echo exec("/bin/ping -c 4 ".$addr);
                            }else{
                            die("Неверный формат, обнаруженны запрещенные символы!");
                    }
                 }
                }
                ?>
                </b>
              </div>
          </div>
        </div>
        <!-- /.row -->
        <hr>
        <!-- Footer -->
        <footer>
            <div class="row text-center">
                <div class="col-lg-12">
                    <p>Создано инженерами НПО "Эшелон"</p>
					<p class="navbar-brand">
                    <img src="./img/logo.png">
                </p> 
                </div>
            </div>
        </footer>
    </div>
    <!-- /.container -->
    <!-- jQuery -->
    <script src="./js/jquery.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="./js/bootstrap.min.js"></script>
</body>
</html>